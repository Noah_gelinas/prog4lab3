using MemoryGame;

namespace ProjectTests;

[TestClass]
public class UnitTest1
{
    [TestMethod]
    public void TestMethod1()
    {
        Assert.AreEqual(1,1);
    }


    [TestMethod]
    public void AddPoints_NewPlayer()
    {
        Player p = new("Andrew");
        p.addPoint();
        Assert.AreEqual(1,p.Points);
    }
}