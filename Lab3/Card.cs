namespace MemoryGame{
    public class Card{
        public Card(CardValue value){
            Value = value;
        }

        //The Value property is the symbol on it, defined by CardValue
        public CardValue Value { get; }

        //Method to check that two cards Match
        public Boolean Match(Card c){
            return c.Value == this.Value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }

    public enum CardValue{
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
    }
}