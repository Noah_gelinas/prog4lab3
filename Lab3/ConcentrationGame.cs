namespace MemoryGame
{
    public class ConcentrationGame
    {

        private ConcentrationGrid _grid;
        private Player _activePlayer;
        private Player _inactivePlayer;
        public static void Main(string[] args)
        {
            ConcentrationGame game = new ConcentrationGame();
            game.RunGame();
        }


        //The constructor sets up a new game of Concentration
        public ConcentrationGame()
        {
            _grid = new ConcentrationGrid();
            Console.WriteLine("Welcome to Concentration, the matching game!");
            Console.WriteLine("Player 1, enter your name:");
            _activePlayer = new Player(GetValidInput());
            Console.WriteLine("Player 2, enter your name:");
            _inactivePlayer = new Player(GetValidInput());
        }

        //Get non-null input
        private string GetValidInput()
        {
            string? input;
            do
            {
                input = Console.ReadLine();
                
                if(input.Equals("null")){
                    input=null;
                }

            } while (input == null);
            return input;
        }


        private void RunGame()
        {
            while (!_grid.IsEmpty())
            {
                Boolean madeMatch = DoTurn(_activePlayer);
                if (madeMatch)
                {
                    _activePlayer.addPoint();
                    Console.WriteLine($"{_activePlayer.Name} made a match! They now have {_activePlayer.Points} points");
                }
                else
                {
                    Console.WriteLine($"Sorry, no match! It is now {_inactivePlayer.Name}'s turn.");
                    SwitchActivePlayer();

                }
                Console.ReadKey();
            }
            ShowResults();
        }

        internal Boolean DoTurn(Player p)
        {
            //Create List of Guesses;
            List<Tuple<int, int>> guesses = new List<Tuple<int, int>>();
            //Ask for first guess, add it to the list
            guesses.Add(GetGuess(p, guesses));
            //Ask for second guess, add it to the list
            guesses.Add(GetGuess(p, guesses));
            Console.Clear();
            _grid.PrintGrid(guesses);
            //Check to see if it Matches, return the result
            return _grid.CheckMatch(guesses[0], guesses[1]);
        }

        //Prompt the user to enter a guess.
        internal Tuple<int, int> GetGuess(Player p, List<Tuple<int, int>> guesses)
        {
            Console.Clear();
            Console.WriteLine($"{p.Name}, choose a row and column to guess");
            _grid.PrintGrid(guesses);
            Console.Write("Row: ");
            int row=-1;
            while(row<0 || row>3){
                try{
                    row = Convert.ToInt32(GetValidInput());
                }catch(Exception){
                    Console.WriteLine("Row must be between 0 and 3");
                }
            }
            Console.Write("Col: ");
<<<<<<< HEAD
            int column=-1;
            while(column<0 || column >3){
                try{
                    column = Convert.ToInt32(GetValidInput());
                }catch(Exception){
                    Console.WriteLine("Col must be between 0 and 3");
                }
            }
=======
            int column = Convert.ToInt32(GetValidInput());
>>>>>>> 68dcad556d0aee3acfa57494e2c8415324b80025
            return new Tuple<int, int>(row, column);
        }

        //Swaps the currently active player, making the inactive player active and vice versa
        internal void SwitchActivePlayer()
        {
            Player temp = _activePlayer;
            _activePlayer = _inactivePlayer;
            _inactivePlayer = temp;
        }

        private void ShowResults()
        {
            Console.Clear();
            Console.WriteLine($"Game over!");
            Console.WriteLine($"{_activePlayer.Name} has {_activePlayer.Points} points");
            Console.WriteLine($"{_inactivePlayer.Name} has {_inactivePlayer.Points} points");
            if (_activePlayer.Points > _inactivePlayer.Points)
            {
                Console.WriteLine($"{_activePlayer.Name} wins!");
            }
            else
            {
                Console.WriteLine($"{_inactivePlayer.Name} wins!");
            }
        }
    }
}